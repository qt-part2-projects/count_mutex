#include <QtCore/QCoreApplication>

#include <QDebug>
#include <QTime>
#include <QThread>

#define NUM_LOOPS 10000000
qint64  g_sum = 0;

void counting_function(int offset){
  for(auto i = 0; i < NUM_LOOPS; i++){
    g_sum+=offset;
  }
}


int main(int argc, char *argv[])
{
  QCoreApplication a(argc, argv);

  //1. no threads
  QTime t = QTime::currentTime();

  counting_function(1);
  counting_function(-1);
  qDebug()<<"Sum ="<<g_sum;

  qDebug()<<t.msecsTo(QTime::currentTime()) << "msec";

  return a.exec();
}
